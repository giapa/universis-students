import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { LocalizedDatePipe } from '@universis/common';

import {ProfileHomeComponent} from './profile-home.component';
import {ProfilePreviewComponent} from '../profile-preview/profile-preview.component';
import {SharedModule} from '@universis/common';
import {UserService} from '@universis/common';
import {MostModule} from '@themost/angular';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { ConfigurationService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileService} from '../../services/profile.service';
import {BsModalService, ComponentLoaderFactory, ModalModule, PositioningService} from 'ngx-bootstrap';
import {TestingConfigurationService} from '../../../test';
import { LoadingService } from '@universis/common';
import {RouterTestingModule} from '@angular/router/testing';

describe('ProfileHomeComponent', () => {
    // tslint:disable-next-line:prefer-const
    let component: ProfileHomeComponent;
    // tslint:disable-next-line:prefer-const
    let fixture: ComponentFixture<ProfileHomeComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [ProfileHomeComponent,
                ProfilePreviewComponent],
            imports: [
                ModalModule,
                RouterTestingModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                BsModalService,
                ProfileService,
                UserService,
                LoadingService,
                ComponentLoaderFactory,
                PositioningService
            ]
        }).compileComponents();
    }));

});
