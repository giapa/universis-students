import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplyComponent } from './apply/apply.component';
import { ModifyComponent } from './modify/modify.component';
import { PreviewComponent } from './preview/preview.component';

const routes: Routes = [
  {
    path: 'apply',
    component: ApplyComponent
  },
  // {
  //   path: ':id/modify',
  //   component: ModifyComponent
  // },
  {
    path: ':id/preview',
    component: PreviewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ExamPeriodParticipateActionRoutingModule { }
